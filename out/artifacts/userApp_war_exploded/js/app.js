angular.module("webstorm",[]);

angular.module('userApp',['userApp.filters','userApp.services','userApp.directives','userApp.controllers']).
    config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/user-list',{templateUrl:'partials/user-list.html',controller:'UserListCtrl'});
      $routeProvider.when('/user-detail/:id',{templateUrl:'partials/user-detail.html',controller:'UserDetailCtrl'});
      $routeProvider.when('/user-creation',{templateUrl:'partials/user-creation.html',controller:'UserCreationCtrl'});
      $routeProvider.otherwise({redirectTo:'/user-list'});
    }]);