/**
 * Created by 7A9877 on 2014/12/25.
 */
var services = angular.module('userApp.services',['ngResource']);

services.factory('UsersFactory', function ($resource) {
  return $resource('/userApp/web/users',{},{
    query:{method:'GET', isArray:true},
    create:{method:'POST'}
  })
});

services.factory('UserFactory', function ($resource) {
  return $resource('/userApp/web/users/:id,',{},{
    show:{method:'GET'},
    update:{method:'PUT', params:{id:'@id'}},
    delete:{method:'DELETE', params:{id:'@id'}}
  })
});