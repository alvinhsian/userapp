package com.user.controller;

import com.user.model.Userdata;
import com.user.service.UserdataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.lang.model.element.Name;
import java.util.List;
import java.util.Map;

/**
 * STEP:7
 * Created by 7A9877 on 2015/1/12.
 * http://www.journaldev.com/2552/spring-restful-web-service-example-with-json-jackson-and-client-program
 * http://blog.caesarchi.com/2011/08/xmlhttprequestajax.html
 */
@Controller
public class UserdataController {
    @Autowired
    private UserdataService userdataSvc;
    private HttpHeaders httpHeaders = null;
    @RequestMapping(value = RestURIConstants.CREATE_USER, method = RequestMethod.POST)
    @ResponseBody
    public void create(@RequestBody List<Map<String, Object>> mapList) throws Exception{
        userdataSvc.createUser(mapList);
    }

    @RequestMapping(value = RestURIConstants.UPDATE_USER, method = RequestMethod.POST, headers="Accept=application/json")
    @ResponseBody
    public List<Userdata> update(@RequestBody List<Map<String, Object>> mapList) throws Exception{
        return userdataSvc.updateUser(mapList);
    }

    @RequestMapping(value = RestURIConstants.DELETE_USER, method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable Integer id)throws Exception{
        userdataSvc.deleteUser(id);
    }

    @RequestMapping(value = RestURIConstants.READ_USER, method = RequestMethod.GET, headers="Accept=application/json")
    @ResponseBody
    public ResponseEntity find(@PathVariable Integer id) throws Exception{
        httpHeaders = new HttpHeaders();
        //透過Header打開跨網域存取
        httpHeaders.add("Access-Control-Allow-Origin", "*");
        Userdata userdata = userdataSvc.findById(id);
        return new ResponseEntity(userdata, httpHeaders, HttpStatus.OK);
    }

    @RequestMapping(value = RestURIConstants.GET_BY_EMAIL, method = RequestMethod.POST, headers="Accept=application/json")
    @ResponseBody
    public ResponseEntity email(@RequestParam(value = "email") String email) throws Exception{
        httpHeaders = new HttpHeaders();
        //透過Header打開跨網域存取
        httpHeaders.add("Access-Control-Allow-Origin", "*");
        Userdata userdata = userdataSvc.queryByEmail(email);
        return new ResponseEntity(userdata, httpHeaders, HttpStatus.OK);
    }

    @RequestMapping(value = RestURIConstants.GET_ALL_USER, method = RequestMethod.GET, headers="Accept=application/json")
    @ResponseBody
    public ResponseEntity all() throws Exception{
        httpHeaders = new HttpHeaders();
        //透過Header打開跨網域存取
        httpHeaders.add("Access-Control-Allow-Origin", "*");
        List<Userdata> objList = userdataSvc.getAll();
        return new ResponseEntity(objList, httpHeaders, HttpStatus.OK);
    }
}
