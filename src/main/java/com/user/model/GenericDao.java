package com.user.model;

import org.hibernate.Query;

import java.util.List;
import java.util.Map;

/**
 * STEP:1
 * Created by 7A9877 on 2015/1/9.
 */
public interface GenericDao <T>{

    public T create(T t);

    public T update(T t);

    public void delete(Object id);

    public void saveOrUpdate(T t);

    public T findById(Object id);

    public List<T> getAll();

    public Query createQuery(String hql, Object... params);

    public Query createPageQuery(String hql,int offset,int pageSize, Object... params);

    public List<Map> queryBySql(String sql);
}
