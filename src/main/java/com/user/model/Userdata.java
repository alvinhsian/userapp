package com.user.model;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "Userdata")
public class Userdata implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "email")
    private String email;
//    private Date createdDate;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    @JsonSerialize(using=DateSerializer.class)
//    public Date getCreatedDate() {
//        return createdDate;
//    }
//    public void setCreatedDate(Date createdDate) {
//        this.createdDate = createdDate;
//    }
}
