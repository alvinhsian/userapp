package com.user.model;

import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * STEP:3
 * Created by 7A9877 on 2015/1/12.
 */
public interface UserdataDAO extends GenericDao<Userdata>{
    //置放針對此Entity客製化的Method
    @Transactional
    public Userdata queryByEmail(String email);
}
