package com.user.model.impl;

import com.user.model.GenericDao;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.Type;

import java.util.List;
import java.util.Map;

/**
 * STEP:2
 * Created by 7A9877 on 2015/1/9.
 * http://www.codeproject.com/Articles/251166/The-Generic-DAO-pattern-in-Java-with-Spring-and
 * http://www.codejava.net/frameworks/spring/spring-4-and-hibernate-4-integration-tutorial-part-1-xml-configuration
 * http://stackoverflow.com/questions/25260527/obtaining-entitymanager-in-spring-hibernate-configuration
 */
public abstract class GenericDaoImpl<T> implements GenericDao<T> {
    @PersistenceContext
    protected EntityManager entityManager;
    private Class<T> type;

    public GenericDaoImpl(){
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }
    @Transactional
    public Session getSession(){
        Session session = entityManager.unwrap(Session.class);
        return session;
    }

    @Override
    @Transactional
    public T create(final T t) {
        this.entityManager.persist(t);
        return t;
    }

    @Override
    @Transactional
    public T update(final T t) {
        return this.entityManager.merge(t);
    }

    @Override
    @Transactional
    public void delete(Object id) {
        this.entityManager.remove(this.entityManager.getReference(type, id));
    }

    @Override
    @Transactional
    public void saveOrUpdate(T t) {
        getSession().merge(t);
    }

    @Override
    @Transactional
    public T findById(Object id) {
        return (T)this.entityManager.find(type, id);
    }

    @Override
    @Transactional
    public List<T> getAll() {
        List<T> listElements = (List<T>) getSession().createCriteria(type).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return listElements;
    }

    @Override
    @Transactional
    public org.hibernate.Query createQuery(String hql, Object... params) {
        org.hibernate.Query query = getSession().createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return query;
    }

    @Override
    @Transactional
    public org.hibernate.Query createPageQuery(String hql, int offset, int pageSize, Object... params) {
        org.hibernate.Query query = getSession().createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        query.setFirstResult(offset).setMaxResults(pageSize);
        return query;
    }

    @Override
    @Transactional
    public List<Map> queryBySql(String sql) {
        Query query = entityManager.createQuery(sql);
        return (List<Map>) query.getResultList();
    }
}
