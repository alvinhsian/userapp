package com.user.model.impl;

import com.user.model.Userdata;
import com.user.model.UserdataDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * STEP:4
 * Created by 7A9877 on 2015/1/12.
 */
@Repository("UserdataDAO")
public class UserdataDAOImpl extends GenericDaoImpl<Userdata> implements UserdataDAO {
    public Userdata queryByEmail(String email) {
        Session session = super.getSession();
        Query query = session.createQuery("from com.user.model.Userdata user where user.email=:email");
//        String email = map.get("email").toString();
        query.setParameter("email",email);
        List<Userdata> userdataList = query.list();
        Userdata userdata = new Userdata();
        for (Userdata userdata1 : userdataList) {
            userdata = userdata1;
        }
        return userdata;
    }
//可擴增其他method...
}
