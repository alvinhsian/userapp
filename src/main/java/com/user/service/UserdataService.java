package com.user.service;

import com.user.model.Userdata;
import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * STEP:5
 * Created by 7A9877 on 2015/1/12.
 * Transactional應在Service層被實作
 */
public interface UserdataService {

    @Transactional
    public void createUser(List<Map<String, Object>> mapList);

    @Transactional
    public List<Userdata> updateUser(List<Map<String, Object>> mapList);

    @Transactional
    public void saveOrUpdate(Userdata userdata);

    @Transactional
    public void deleteUser(Integer id);

    @Transactional
    public Userdata findById(Integer id);

    @Transactional
    public List<Userdata> getAll();

    @Transactional
    public List<Map> queryBySql(String sql);

    @Transactional
    public Query createQurey(String hql, Object... params);

    @Transactional
    public org.hibernate.Query createPageQuery(String hql, int offset, int pageSize, Object... params);

    @Transactional
    public Userdata queryByEmail(String email);
    //...
}
