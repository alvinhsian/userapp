package com.user.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.user.model.Userdata;
import com.user.model.UserdataDAO;
import com.user.service.UserdataService;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * STEP:6
 * Created by 7A9877 on 2015/1/12.
 */
@Service("UserdataService")
public class UserdataServiceImpl implements UserdataService {
    @Autowired
    private UserdataDAO userdataDAO;

    @Override
    public void createUser(List<Map<String, Object>> mapList) {
        ObjectMapper mapper = new ObjectMapper();
        Userdata userdata = null;
        for (Map<String, Object> map : mapList) {
            userdata = mapper.convertValue(map, Userdata.class);
            userdataDAO.create(userdata);
        }
    }

    @Override
    public List<Userdata> updateUser(List<Map<String, Object>> mapList) {
        ObjectMapper mapper = new ObjectMapper();
        Userdata userdata = null;
        List<Userdata> list = new ArrayList<Userdata>();
        for (Map<String, Object> map : mapList) {
            userdata = mapper.convertValue(map, Userdata.class);
            list.add(userdataDAO.update(userdata));
        }
        return list;
    }

    @Override
    public void saveOrUpdate(Userdata userdata) {
        userdataDAO.saveOrUpdate(userdata);
    }

    @Override
    public void deleteUser(Integer id) {
        userdataDAO.delete(id);
    }

    @Override
    public Userdata findById(Integer id) {
        Userdata userdata = userdataDAO.findById(id);
        return userdata;
    }

    @Override
    public List<Userdata> getAll() {
        return userdataDAO.getAll();
    }


    @Override
    public List<Map> queryBySql(String sql) {
        return userdataDAO.queryBySql(sql);
    }

    @Override
    public Query createQurey(String hql, Object... params) {
        return userdataDAO.createQuery(hql, params);
    }

    @Override
    public Query createPageQuery(String hql, int offset, int pageSize, Object... params) {
        return userdataDAO.createPageQuery(hql, offset, pageSize, params);
    }

    @Override
    public Userdata queryByEmail(String email) {
        Userdata userdata = userdataDAO.queryByEmail(email);
        return userdata;
    }
}
