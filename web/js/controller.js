/**
 * Created by 7A9877 on 2014/12/25.
 */
var app = angular.module('userApp.controllers',[]);

//app.service('xxxService', function($http){
//  this.listUsers = function(group, callback){ http.get('' + group).success(callback)}
//});
app.controller('UserListCtrl', function ($scope, UsersFactory, UserFactory, $location) {
    // callback for ng-click 'editUser':
    $scope.editUser = function(userId){
      $location.path('/user-detail/'+userId);
    };

  //$scope.queryUsers = function(group){
  // xxxService.listUsers($scope.group, function(data){})
  //
  //};

    // callback for ng-click 'createUser':
    $scope.deleteUser = function (userId) {
      UserFactory.delete({id:userId});
      $scope.users = UsersFactory.query();
    };

    // callback for ng-click 'createUser':
    $scope.createNewUser = function () {
      $location.path('/user-creation');
    };
    $scope.users = UsersFactory.query();
});

app.controller('userDetailCtrl', function ($scope, $routeParams, UserFactory, $location) {
    // callback for ng-click 'updateUser':
    $scope.updateUser = function () {
      UserFactory.update($scope.user);
      $location.path('/user-list');
    };

    // callback for ng-click 'cancel':
    $scope.cancel = function () {
      $location.path('/user-list');
    };
    $scope.user = UserFactory.show({id:$routeParams.id});
  });

app.controller('UserCreationCtrl', function ($scope, UsersFactory, $location) {
    // callback for ng-click 'createNewUser':
    $scope.createNewUser = function () {
      UsersFactory.create($scope.user);
      $location.path('/user-list');
    }
  });